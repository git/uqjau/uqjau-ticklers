#!/usr/bin/env bash

help() {
sed -n " 2,\$ {s/\$ourname/${0##*/}/g; s/^    //; p;}" <<\____eohd

    SYNOPSIS

        ourname [OPTIONS] [TIME_SPEC] [REMINDERWORD]...

    DESCRIPTION

        Single-shot tickler/alarm; echos out reminder
        phrase to current tty at specified time or after specified delay.
    
    OPTIONS

        -b         run in background
   
        -w         list recent logs
    
    LOG

        in ~/tmp/ if it exists, otherwise in /tmp
    
    ENV

        If env var '_wall' is set to anything, then wall is also used
        to send the reminder phrase.

        If env var append_to_doing_journal is set to anything, run doing with
        reminder msg.
    
    NOTES

        Debugging tips:

            _psg remind.console
            ourname -w

        TBD create a self test that exercises the crucial cases

        Ex 
            $ourname . eat lunch  # default delay used
            $ourname  # default delay, and default alert used
            $ourname "now +45m" take break
            $ourname "11 sec" GO
            $ourname 16m go home now
            $ourname "1am tue" goto bed
            $ourname 17:55 go home
   
        hint: run this in the background in the tty 
        that your working in most

    --------------------------------------------------------------------
    Rating: tone: handy tool used: often stable: y TBDs: n noteable: y
    --------------------------------------------------------------------

    UPDATED: _m4_date
____eohd

    : :end 'help()'
}

[[ ! ${1:-} == --help ]] || { help; exit; }

# ====================================================================
# Copyright (c) 2009,2010,2011,2013,2017 Tom Rodman <Rodman.T.S@gmail.com>
# --------------------------------------------------------------------

# -- Software License --
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---

ourname=${0##*/}
ourdir=${0%/$ourname}
  # may be useful dir for "rc" config files
Ourname=${0}

# ==================================================
# parse commandline switches
# ==================================================
opt_true=1 opt_char= badOpt=
OPTIND=1
  # OPTIND=1 for 2nd and subsequent getopt invocations; 1 at shell start

OPT_b= OPT_w=
while getopts bw opt_char                               # no leading : => normal getopts error reporting
do
    # save info in an "OPT_*" env var.
    [[ $opt_char != \? ]] && eval OPT_${opt_char}="\"\${OPTARG:-$opt_true}\"" ||
        badOpt=1
done
shift $(( $OPTIND -1 ))

# If badOpt:  If in function return 1, else exit 1:
[[ -z $badOpt ]] || { : help; return 1 &>/dev/null || exit 1; }

unset opt_true opt_char badOpt
##

if [[ -d ~/tmp ]];then
    tmplogdir=~/tmp/$ourname
else
    tmplogdir=/tmp/$USER.$ourname
fi
mkdir -p $tmplogdir

if [[ -n $OPT_w ]];then

    # List recent logs and exit.

    # ex log: remind.console.tty13.move_on_.16447.log

    recent="$(find $tmplogdir/*$ourname.* -mmin -360)"
    [[ -n $recent ]] || { echo $ourname: NO recent logs. >&2; exit; }

    #(set -x;ls -ldrtog $recent)

    echo "$recent"|_sortfiles_find_or_filter -R -
    
    exit
fi

if [[ -n $OPT_b ]];then

    # background ourselves

    unset ans
    if [[ $# == 0 ]];then
        read -p 'Will group your input as one arg. > ' ans
        [[ -z $ans ]] || set -- "$ans"
    fi

    mytty=$(tty 2>&1 || true)     $0 "$@" &
    exit
else
    mytty=${mytty:-$(tty 2>&1 || true)}
fi

append_to_doing_journal=

# You might set env vars
#    "default_nap2" 
#    "append_to_doing_journal"
# in the rcfile.

# --------------------------------------------------------------------
# source ~/.${ourname}rc if it exists
# --------------------------------------------------------------------
rcfile=$HOME/.${ourname}rc
  # env vars impacted by rcfile:
  #   imp!! LIST_VARS_CHANGED_HERE
if test -s "$rcfile"
then
  [[ -n $_verbose ]] && set -x
  source "$rcfile"
  [[ -n $_verbose ]] && set +x
fi
unset rcfile


show_reminder() {
    local reminder="$alarm_banner"$'\n'"# $(date)"$'\n'"$msg"$'\n'"$alarm_banner"

    if [[ -n ${_wall:-} ]];then
        echo -e "$FUNCNAME:$(date):About to pipe to '_pwall'.\n---" >> $log

        tee -a $log <<<"$reminder" |_pwall
        return
    else
        # show reminder text on STDOUT (also to $log)
        echo -e "$FUNCNAME:$(date):About to write to 'STDOUT'.\n---" >> $log

        tee -a $log >&3  <<<"$reminder"
    fi
}

bell_alert() {
    local bells=6
    local delay=4.2s

    for (( i=1; $i < $bells ;i += 1));
    do 
        echo -en '\a' >&3
        sleep $delay
    done
}

# main
set -e
default_nap2="${default_nap2:-now +12min}"

#if test -n "${bknap2:-}"
#then
#  set "${bknap2}" "$@"
#fi

#--
# support two short hand notations for minutes
    # 10m => 10min
    # 10  => 10min

shopt -s extglob
    # "extended  pattern  matching operators" for upcoming 'case' statement
    #   man bash|col -b|less +/'If the extglob shell option' ##

case $1 in
    +([0-9])m)
        # 17m   matches the regex
        # 17min does NOT match the regex
        tmp="now +${1}in"
            # m becomes 'min'
        shift
        set "$tmp" "$@"

        # so you can say
        #   $0 3m "you are done" &
    ;;
    +([0-9]))
        tmp="now +${1}min"
            # apppending default unit
        shift
        set "$tmp" "$@"
    ;;
esac

case $# in
    0)
        nap2_arg=$default_nap2
    ;;
    *)
        case $1 in
            .)
                nap2_arg=$default_nap2
                # '.' as 1st arg => default the delay
                shift
            ;;
            *)

                if ! poss_err=$(date --date "$1"  2>&1); then
                    # Test case:  ourname 'now +2.2 hours' lunch

                    echo "WARNING: Ignoring \$1 as time spec, because [date --date \"$1\"] said: [$poss_err]" >&2
                    echo $ourname: using default_nap2 value: $default_nap2 >&2
                    echo

                    nap2_arg=$default_nap2
                else
                    nap2_arg=$1
                    shift
                fi
            ;;
        esac
    ;;
esac

remind_text_default="move on"
remind_text="${*:-$remind_text_default}"

safe_for_filename() {

    perl -pe '
        s~\s+~_~g;
        s~;~=073=~g;
        s~:~=072=~g;
        s~/~=057=~g;
        s~\n~s=015=s~msg;
    '
}

remind_text_for_log=$(
    safe_for_filename <<<"$remind_text"
)

# -------------------------------------------------------------------- 
# setup log
# -------------------------------------------------------------------- 

if [[ -n ${mytty:-} ]];then
    tty=tty${mytty##*/}
else
    tty=
fi

log=$tmplogdir/"$ourname.${tty:+$tty.}$remind_text_for_log.$$.log"
    # ex log:
    #   $ cat remind.console.tty13.move_on_.16447.log
    #   started: [Tue Aug 20 05:01:56 CDT 2013] on [/dev/pts/13]
    #   nap2_arg: [now +3min]
    #   remind_text: [move on]
    #   sleeping until: [Tue Aug 20 05:04:56 CDT 2013]

log_longterm=$HOME/log/$ourname
mkdir -p $HOME/log

    # ex $log_longterm
    #   $ tail log/remind.console
    #   started: [Tue Aug 20 04:58:38 CDT 2013] on [/dev/pts/13]
    #   nap2_arg: [now +12min]
    #   remind_text: [move on]
    #   sleeping until: [Tue Aug 20 05:10:38 CDT 2013]
    # 
    #   started: [Tue Aug 20 05:01:56 CDT 2013] on [/dev/pts/13]
    #   nap2_arg: [now +3min]
    #   remind_text: [move on]
    #   sleeping until: [Tue Aug 20 05:04:56 CDT 2013]

if [[ -n $append_to_doing_journal
      && $remind_text != $remind_text_default ]];then

    doing "$remind_text"
fi

exec 3>&1 
    # save STDOUT pointer for later
trap : SIGTTOU 
    ## allows backgrounding for bash/linux
exec >/dev/null  2>&1 
    ## allows backgrounding for tcsh (SUN OS)

# ====================================================================== 
## main
# ====================================================================== 

started=$(date)

msg=$(cat <<___eohd
# $ourname $nap2_arg
#    ## ${remind_text:-} ##
#   ### ${remind_text:-} ###
#   started: [$started]
___eohd
)

alarm_banner='# ALARM-ALARM-ALARM-ALARM-ALARM-ALARM-ALARM-ALARM-ALARM-ALARM-ALARM'

{
    echo "started: [$started] on [$mytty]"
    echo "nap2_arg: [$nap2_arg]"
    echo "remind_text: [$remind_text]"

    nap2 "$nap2_arg" 

} &>$log &

## delay is in progress now

sleep .5s
  # give 'nap2' script some time to output time string to $log
cat $log >&3
(cat $log; echo) >> $log_longterm

## create symbolic link for log w/done time in symbolic link
sleep_until=$(
    perl -ne '
        print "$1\n" if ( m{sleeping until: \[(.*)\]} );
    ' $log
)

[[ -z $sleep_until ]] ||
    ln -s $log $log.$(date --date "$sleep_until" '+%y.%m%d.%H%M.%S')
        # ex symb link: 
        #   remind.console.tty3.now_lawsuite..._next_dishes,_then_postal_mail_then_walk_.30154.log.14.1217.0716.32

##

wait
  # wait for 'nap2' to finish
bell_alert &
sleep .15

show_reminder

lognew=${log/$ourname/$ourname.DONE}
mv $log ${lognew}
