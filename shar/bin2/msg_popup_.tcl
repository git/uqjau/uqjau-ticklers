#!/usr/bin/env bash

# --------------------------------------------------------------------
# Synopsis: show message in GUI/popup window
#   discretely - require 1 button push to reveal contents
# --------------------------------------------------------------------
# Usage: $ourname [-b BUTTONLABEL] MESSAGE
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) 2009,2010 Tom Rodman <Rodman.T.S@gmail.com>
# --------------------------------------------------------------------

# -- Software License --
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---

# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2010/11/07 16:47:01 $   (GMT)
# $Revision: 1.1 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker: rodmant $
#   $Source: /usr/local/7Rq/package/cur/ticklers-2009.05.14/shar/bin/RCS/msg_popup_.tcl,v $
#      $Log: msg_popup_.tcl,v $
#      Revision 1.1  2010/11/07 16:47:01  rodmant
#      Initial revision
#
# --------------------------------------------------------------------

# ==================================================
# parse commandline switches
# ==================================================
opt_true=1 OPTIND=1
  # OPTIND=1 for 2nd and subsequent getopt invocations; 1 at shell start

while getopts b: opt_char
do
   # save info in an "OPT_*" env var.
   test "$opt_char" != \? && eval OPT_${opt_char}="\"\${OPTARG:-$opt_true}\"" || false
done
shift $(( $OPTIND -1 ))
unset opt_true opt_char

export _msg=${1:-no message given}
button_label=${OPT_b:-fortune}

## main

# tcl/tk code largely from "wiki" website
#  http://mini.net/tcl/0.html

/usr/bin/env wish <<%%%
#! /usr/bin/wish
button .b -text "${button_label}" -command {.b config -text "${MSG:-$_msg}"}
 pack   .b ;#RS
%%%
